<?php

namespace Drupal\stats\Plugin;

/**
 * Base class for Stat source plugins.
 */
abstract class StatSourceBase extends StatPluginBase implements StatSourceInterface {

}
