<?php

namespace Drupal\stats\Plugin;

/**
 * Base class for Stat destination plugins.
 */
abstract class StatDestinationBase extends StatPluginBase implements StatDestinationInterface {

}
